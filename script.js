// task 1

const strings = ["travel", "hello", "eat", "ski", "lift"];
const filteredStrings = strings.filter((str) => str.length > 3);
const count = filteredStrings.length;

console.log(`Кількість рядків з довжиною більше за 3 символи: ${count}`);

// tasks 2

// const people = [
//     { name: "Іван", age: 25, sex: "чоловіча" },
//     { name: "Марія", age: 30, sex: "жіноча" },
//     { name: "Петро", age: 22, sex: "чоловіча" },
//     { name: "Олена", age: 28, sex: "жіноча" }
// ];

// const malePeople = people.filter((person) => person.sex === "чоловіча");

// console.log(malePeople);

// task 3

// function filterBy(arr, dataType) {
//     return arr.filter((item) => typeof item !== dataType);
// }

// const myArray = ['hello', 'world', 23, '23', null];
// const filteredArray = filterBy(myArray, 'string');
// console.log(filteredArray); 

