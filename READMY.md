1) Метод forEach виконує певну функцію один раз для кожного елемента в масиві.
Приклад:

const array = ['a', 'b', 'c'];
array.forEach((element) => {
    console.log(element);
});

2) Методи, які мутують існуючий масив:

push(): Додає один або декілька елементів в кінець масиву.
const arr = [1, 2, 3];
arr.push(4, 5);
console.log(arr);

pop(): Видаляє останній елемент з масиву.
const arr = [1, 2, 3];
const removed = arr.pop();
console.log(arr); 
console.log(removed); 

shift(): Видаляє перший елемент з масиву.
const arr = [1, 2, 3];
const removed = arr.shift();
console.log(arr); 
console.log(removed); 

unshift(): Додає один або декілька елементів на початок масиву.
const arr = [2, 3];
arr.unshift(0, 1);
console.log(arr);

splice(): Видаляє, замінює або додає елементи в середину масиву.
const arr = [1, 2, 3, 4, 5];
arr.splice(1, 2); 
console.log(arr); 

Методи, які повертають новий масив:

concat(): Повертає новий масив, який містить поточний масив та доповнений іншими масивами та елементами.
const arr1 = [1, 2];
const arr2 = [3, 4];
const combined = arr1.concat(arr2, 5, 6);
console.log(combined); 

slice(): Повертає новий масив, який є частиною вихідного масиву.
const arr = [1, 2, 3, 4, 5];
const sliced = arr.slice(1, 4); 
console.log(sliced); 

map(): Повертає новий масив, який формується на основі заданої функції перетворення.
const arr = [1, 2, 3];
const doubled = arr.map((num) => num * 2);
console.log(doubled); 

filter(): Повертає новий масив, що містить елементи, які задовільняють умову, задану в тестовій функції.
const numbers = [7, 2, 9, 4, 6, 3];
const filteredNumbers = numbers.filter((number) => number > 5);
console.log(filteredNumbers);

3) Метод Array.isArray() перевіряє, чи передане значення є масивом, і повертає true, якщо це так, або false, якщо це не масив.

const myVariable = [1, 2, 3];

if (Array.isArray(myVariable)) {
    console.log('Змінна є масивом.');
} else {
    console.log('Змінна не є масивом.');
}

4) Метод map() використовують, коли потрібно створити новий масив на основі вихідного, змінивши значення елементів.
map() повертає новий масив, який містить результати виклику заданої функції для кожного елемента вихідного масиву.

Метод forEach() використовують, коли потрібно просто виконати певну дію для кожного елемента масиву без створення нового масиву.
forEach() не повертає новий масив, а лише ітерує по елементам.









